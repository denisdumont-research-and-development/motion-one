<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Baluchon</title>
    <link rel="icon" href="./dist/images/baluchon.svg">
    <link rel="stylesheet" href="./dist/css/app.css">
    <script src="./dist/js/app.js" defer></script>
</head>
<body>

<? $classes = 'loader-circle bottom-0 right-0 w-screen aspect-square rounded-full fixed z-50 odd:bg-primary
even:bg-black bottom-[-50vh] right-[-50vw]'; ?>

<div class="w-screen h-screen fixed top-0 left-0 flex flex flex-col z-50">
    <div class="w-full h-full odd:bg-primary even:bg-darkPrimary loader-line"></div>
    <div class="w-full h-full odd:bg-primary even:bg-darkPrimary loader-line"></div>
    <div class="w-full h-full odd:bg-primary even:bg-darkPrimary loader-line"></div>
    <div class="w-full h-full odd:bg-primary even:bg-darkPrimary loader-line"></div>
    <div class="w-full h-full odd:bg-primary even:bg-darkPrimary loader-line"></div>
    <div class="w-full h-full odd:bg-primary even:bg-darkPrimary loader-line"></div>
</div>


<div class="fixed top-5 right-5 -rotate-90 progress-container z-30">
    <svg width="50" height="50" viewBox="0 0 100 100">
        <circle cx="50" cy="50" r="30" pathLength="1" class="stroke-darkPrimary"/>
        <circle cx="50" cy="50" r="30" pathLength="1" class="progress stroke-primary"/>
    </svg>
</div>

<main class="container">
    <div class="flex flex-col lg:flex-row gap-10 lg:h-screen">
        <div class="aspect-square w-full lg:w-1/2 px-10 relative">
            <div class="flower-circle aspect-square w-2/3 rounded-full bg-primary absolute top-1/2 left-1/2
-translate-y-full -translate-x-1/2 z-before scale-0 opacity-0"></div>
            <svg class="w-full h-full object-contain animated-svg" viewBox="0 0 2457 764">
                <path pathLength="1" class="stroke-darkGreen"
                      d="M1 704.763C84.6667 699.596 283.4 696.563 409 725.763C534.6 754.963 905.667 707.263 1075.5
                679.763C1102.17 670.263 1155.2 644.563 1154 617.763C1152.5 584.263 1131.5 263.263 1103.5 292.763C1075.5 322.263 930.5 312.763 877 213.263C834.2 133.663 832.167 129.763 836.5 137.763L1103.5 288.263C1109.67 250.596 1105.6 170.663 1040 152.263C974.4 133.863 875.333 133.929 834 136.263L1116.5 297.263C1195.83 276.096 1244.5 213.263 1379 51.0001C1493.46 -87.0804 1200.94 98.8995 1167 135C1112 193.5 1115.17 268.263 1111.5 300.263M2456.5 617.763C1762 700.763 1519 500.763 1270 617.763C1021 734.763 1169 792.263 1141.5 748.763C1119.5 713.963 1111 436.929 1109.5 302.763C1115.5 297.263 1167 118 1406 2"
                      stroke="black"/>
            </svg>
        </div>
        <div class="relative w-full lg:w-1/2 h-full flex flex-col justify-center py-10 lg:py-0 px-10 before:w-full
        lg:before:w-[50vw]
        before:absolute before:h-full before:z-before before:top-0 before:left-0 before:block before:bg-darkPrimary">
            <h1 class="text-white pb-6">
                <span class="text-primary ">Flower </span>Experience
            </h1>
            <p class="text-white">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam condimentum lobortis malesuada. Aenean
                tortor justo, vestibulum a venenatis ut, rhoncus ut lectus. Donec fermentum, nisl ut ullamcorper tempus,
                diam nunc pretium nibh, a eleifend justo nisl quis ex. Nunc sit amet luctus magna, id fermentum neque.
                In hac habitasse platea dictumst.
            </p>
        </div>
    </div>
    <div class="py-20 flex flex-col lg:flex-row">
        <div class="w-full lg:w-2/3 px-5">
            <h2>Lorem ipsum dolor sit amet</h2>
            <p>Vivamus iaculis lacus lectus. Praesent rhoncus ac neque sit amet sollicitudin. Phasellus quis orci id
                nisi
                iaculis rhoncus eu sollicitudin quam. Cras mi sapien, condimentum et odio egestas, consectetur placerat
                lacus. Quisque efficitur, justo in tempor vehicula, ex nunc mattis purus, vitae dapibus lectus tellus a
                urna. Nunc id enim non risus faucibus aliquet. Pellentesque lacinia laoreet ante at auctor. Quisque
                vehicula
                turpis dolor, sit amet fermentum risus dapibus et. Duis dictum varius condimentum. Quisque in urna leo.
                Nullam vestibulum sem ac neque malesuada, at fringilla eros fermentum. Etiam id mauris ac enim ultricies
                ullamcorper vel nec neque. Phasellus vel nibh eu nisi fringilla fringilla at ut mauris. Donec eleifend
                euismod posuere.</p>
        </div>
        <div class="w-full lg:w-1/3 grid grid-cols-2 gap-4 px-5 py-10 lg:py-0">
            <div class="aspect-square w-full flower-cell opacity-0">
                <img src="dist/images/flowers-1.jpg" alt="flower-1" class="w-full h-full object-cover object-center">
            </div>
            <div class="aspect-square w-full flower-cell opacity-0">
                <img src="dist/images/flowers-2.jpg" alt="flower-2" class="w-full h-full object-cover object-center">
            </div>
            <div class="aspect-square w-full flower-cell opacity-0">
                <img src="dist/images/flowers-3.jpg" alt="flower-3" class="w-full h-full object-cover object-center">
            </div>
            <div class="aspect-square w-full flower-cell opacity-0">
                <img src="dist/images/flowers-4.jpg" alt="flower-4" class="w-full h-full object-cover object-center">
            </div>
        </div>
    </div>

</main>


</body>
</html>
