module.exports = {
    theme: {
        mode: 'jit',
        extend: {
            aspectRatio: {
                'square': '1/1',
            },
            fontFamily: {
              raleway: ['Raleway', 'sans-serif']
            },
            zIndex: {
                'before': '-1',
            },
            colors: {
                darkPrimary: '#202F23',
                primary: '#C0F397',
            }
        },
    },
    variants: {},
    plugins: [
        require('@tailwindcss/aspect-ratio'),
        require('@tailwindcss/line-clamp'),
    ],
    purge: {
        enabled: false,
        content: [
            './public/index.php',
        ],
    },
}
