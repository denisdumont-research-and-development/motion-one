import Alpine from 'alpinejs'

window.Alpine = Alpine
Alpine.start()

import Flower from "./Components/Flower";
import Loader from "./Components/Loader";
import Progress from "./Components/Progress";
import FlowerCells from "./Components/FlowerCells";

const progress = Progress.init();
const flowerCells = FlowerCells.init();
const loader = Loader.init();

loader.finished.then(() => {
    Flower.init();
    Flower.initCircle();
})


