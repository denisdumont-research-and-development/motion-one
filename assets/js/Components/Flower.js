import {timeline, animate} from "motion";

export default class Flower {

    static init() {
        return timeline([
            ["path", this.draw(1), {duration: 4}],
        ])
    }

    /***
     * Draw Flower
     * @param progress
     * @returns {{visibility: string, strokeDashoffset: number}}
     */
    static draw(progress) {
        return {
            strokeDashoffset: 1 - progress,
            visibility: "visible",
        }
    }

    static initCircle(){
         return  animate(
             ".flower-circle",
             { scale: 1, x: '-50%', y: '-50%', opacity: 100 },
             { duration: 3 })
    }
}