import { animate, scroll } from "motion";

export default class Progress {
    static init(){
        return scroll(animate(".progress", { strokeDasharray: ["0,1", "1,1"] }));
    }
}