import {animate, inView, stagger} from "motion";

export default class FlowerCells{
    static init(){
        return inView(".flower-cell", (info) => {
            animate(info.target, { opacity: 1 }, {duration: 2, delay: stagger(2)})
        })
    }
}