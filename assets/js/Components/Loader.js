import {animate, stagger} from "motion";

export default class Loader {
    static init(){
        return animate(
            ".loader-line",
            {x: '100vw'},
            {
                delay: stagger(0.1),
                duration: 1,
                easing: [.22, .03, .26, 1]
            });
    }
}